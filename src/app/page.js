"use client";
import React, { useState, useRef, useEffect } from "react";
import { Image } from "antd";

export default function Home() {
  const audioRef = useRef(null);
  const [click, setClick] = useState(false);
  
  useEffect(() => {
    const handleEnded = () => {
      setClick(false);
    };

    if (audioRef.current) {
      audioRef.current.addEventListener("ended", handleEnded);
    }

    return () => {
      if (audioRef.current) {
        audioRef.current.removeEventListener("ended", handleEnded);
      }
    };
  }, []);

  const handlePlayPause = () => {
    audioRef.current.currentTime = 0;
    audioRef.current.play();
    setClick(true);
  };

  return (
    <div className="flex justify-center items-center mt-10">
      <div>
        <div className="flex justify-center items-center w-full">
          <Image
            onClick={handlePlayPause}
            width={200}
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKbsod4MZi8EmIlh7fqRHyKzFVvSitwx42m02b9fUYOA&s"
            preview={{
              src: "https://static.vecteezy.com/system/resources/previews/003/394/652/original/i-love-you-sticker-illustration-free-vector.jpg",
            }}
            alt="pic"
          />
        </div>
        {click && (
          <img
            className="fixed z-[9999999] w-44 xl:w-96 top-5 left-2 xl:top-40 xl:left-10"
            src="https://media.tenor.com/D_si8GTbHK8AAAAi/cat-cats.gif"
            alt="cat-dance"
          />
        )}
      </div>
      <audio className="hidden" ref={audioRef} controls src="../kimi.mp3">
        Your browser does not support the audio element.
      </audio>
    </div>
  );
}

